
Role Validator
============

Features:

* Validate user role

### Install


### Using Role Validator
    from validator import RoleValidator
    role = RoleValidator()
    role.init_app(app)

### Token Getter
    @role.tokengetter
    def get_token(token):
        """
        Return User json from token
        """
        token = Token.query.filter_by(token=token).first()
        user = User.query.filter_by(id=token.user_id).first()
        return user.to_json()

### Check permission
    @app.route("/post")
    @role.validate("group", "membership")
    def post():
        return
