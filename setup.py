#!/usr/bin/env python
from setuptools import setup

setup(
    name='role_validator',
    author='Giang Nguyen',
    author_email='giangnt@toivui.com',
    description='Validate user permission',
    license="MIT",
    url='https://bitbucket.org/ntgiang/role_validator',
    version='0.1.0',
    packages=['role_validator'],
    test_suite='tests',
    install_requires=(
        'Flask==0.10.1'
    ))
