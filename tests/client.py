# coding: utf-8

from flask import Flask, jsonify, request
from role_validator import RoleValidator

app = Flask(__name__)
app.secret_key = 'secret'

role = RoleValidator()
role.init_app(app)


@role.tokengetter
def get_token(token):
    return current_user

current_user = {
    "id": "12213",
    "user_type": "developer",
    "roles": [
        {"group1": "editor"},
        {"group2": "tester"}
    ]
}


@app.route("/token")
def token():
    token = request.args.get("access_token")
    if token == 1:
        return current_user
    return None


@app.route("/")
@role.validate(group="group1", membership='admin')
def home():
    return jsonify(current_user)


if __name__ == '__main__':
    app.run(debug=True)
