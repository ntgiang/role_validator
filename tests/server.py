from flask import g
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), unique=True, index=True,
                         nullable=False)
    roles = db.Column(db)

    def check_password(self, password):
        return True


class Token(db.Model):
    token = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)


def current_user():
    return g.user


def prepare_app(app):
    db.init_app(app)
    db.app = app
    db.create_all()

    user = User(username='admin', roles=[{"group_1": "admin"}])


