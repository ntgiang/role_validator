from flask import request, abort
from functools import wraps
import logging

log = logging.getLogger("role_validator")


class RoleValidator(object):
    def __init__(self, app=None, tokengetter=None):
        # self.app = app
        # if app:
        #     self.init_app(app)
        self._tokengetter = tokengetter

    def init_app(self, app):
        """
        Init app with Flask app
        :param app:
        :return:
        """
        self.app = app

    def tokengetter(self, f):
        self._tokengetter = f
        return f

    def validate(self, function=None, group=None, membership=None):
        """
        Check user has enough permission
        :param function:
        :param group: group name required
        :param membership: role required
        :return:
        """
        def actual_decorator(function):
            @wraps(function)
            def returned_func(*args, **kwargs):
                token = request.headers.get("X-Token") or \
                        request.values.get("access_token")
                if not token:
                    msg = 'Token headers must be X-Token'
                    log.debug(msg)
                    abort(401)
                user = self._tokengetter(token)
                validate = self.validate_role(user, group, membership)
                if not validate:
                    msg = 'User not enough permission'
                    log.debug(msg)
                    abort(401)
                return function(*args, **kwargs)
            return returned_func

        def waiting_for_func(function):
            return actual_decorator(function)
        return waiting_for_func

    @staticmethod
    def validate_role(user, group, membership):
        if None in (user, group, membership):
            return False
        if "roles" not in user:
            return False
        if user["user_type"] == "admin":
            return True
        role_needed = {group: membership}
        if role_needed not in user["roles"]:
            return False
        return True
